import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'signin.dart';
import 'package:regexed_validator/regexed_validator.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  var _formkey = GlobalKey<FormState>();
  String errorMessage = '';
  String errorMessage1 = '';
  bool _obsecureText = true;
  bool loading = false;

  final _email = TextEditingController();
  final _password = TextEditingController();
  final _name = TextEditingController();

  Future<bool> register(String email, String pass, String name) async {
    FirebaseAuth _auth = FirebaseAuth.instance;

    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: pass);
      FirebaseUser user = result.user;

      UserUpdateInfo info = UserUpdateInfo();
      info.displayName = name;
      user.updateProfile(info);
      return true;
    }
    // on FirebaseAuthUserCollisionException{
    //   setState(() {
    //     errorMessage = 'User with This email Already exist';
    //   });
    // }
    catch (FirebaseAuthUserCollisionException) {
      setState(() {
        errorMessage = 'User with This email Already exist';
        loading = false;
      });
      return false;
    } catch (e) {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.greenAccent,
      body: SafeArea(
        child: Form(
            key: _formkey,
            child: ListView(
              children: <Widget>[
                Stack(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(25, 80, 0, 0),
                      child: Text(
                        'Auth',
                        style: TextStyle(
                          fontSize: 80,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(25, 150, 0, 0),
                      child: Text(
                        'App',
                        style: TextStyle(
                          fontSize: 80,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.white,
                  ),
                  padding: const EdgeInsets.all(20.0),
                  margin: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      TextFormField(
                        validator: (input) {
                          if (input.isEmpty) {
                            return 'Provide a name';
                          }
                        },
                        decoration: InputDecoration(labelText: 'Name'),
                        controller: _name,
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: 'Email Here'),
                        validator: (input) {
                          if (!validator.email(input)) {
                            return 'Please enter correct email address';
                          }
                        },
                        controller: _email,
                        keyboardType: TextInputType.emailAddress,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 85,
                            child: TextFormField(
                              decoration:
                                  InputDecoration(labelText: 'Password'),
                              validator: (input) {
                                if (!validator.mediumPassword(input)) {
                                  return 'Enter a Strong password (A combination of Alphabets and numbers)';
                                }
                              },
                              controller: _password,
                              obscureText: _obsecureText,
                            ),
                          ),
                          Expanded(
                            flex: 15,
                            child: IconButton(
                                icon: Icon(_obsecureText
                                    ? Icons.lock
                                    : Icons.lock_open),
                                onPressed: () {
                                  setState(() {
                                    if (_obsecureText)
                                      _obsecureText = false;
                                    else
                                      _obsecureText = true;
                                  });
                                }),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                        child: Text(
                          errorMessage.isEmpty ? errorMessage1 : errorMessage,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 5,
                    ),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  height: 60,
                  margin: EdgeInsets.fromLTRB(50, 10, 50, 10),
                  child: Material(
                    borderRadius: BorderRadius.circular(30),
                    child: InkWell(
                      child: Center(
                        child: Text(
                          loading ? 'Loading...' : 'Sign Up',
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      onTap: () async {
                        if (_formkey.currentState.validate()) {
                          setState(() {
                            loading = true;
                          });
                          final email = _email.text.toString().trim();
                          final pass = _password.text.toString().trim();
                          final name = _name.text.toString().trim();

                          bool result = await register(email, pass, name);

                          if (result) {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => SignIn()));
                          } else {
                            errorMessage1 = 'Something Wrong Happend';
                            setState(() {
                              loading = false;
                            });
                          }
                        }
                      },
                    ),
                  ),
                ),
                // RaisedButton(
                //   child: Text('Sign Up'),
                //   onPressed: () async {
                //     if (_formkey.currentState.validate()) {
                //       final email = _email.text.toString().trim();
                //       final pass = _password.text.toString().trim();
                //       final name = _name.text.toString().trim();

                //       bool result = await register(email, pass, name);

                //       if (result) {
                //         Navigator.of(context).push(
                //             MaterialPageRoute(builder: (context) => SignIn()));
                //       } else {
                //         errorMessage1 = 'Something Wrong Happend';
                //         print('error');
                //       }
                //     }
                //   },
                // ),

                Container(
                  height: 60,
                  margin: EdgeInsets.fromLTRB(30, 10, 30, 10),
                  child: Material(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(30),
                    child: InkWell(
                      child: Center(
                        child: Text(
                          'Already have account',
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => SignIn()));
                      },
                    ),
                  ),
                ),

                // RaisedButton(
                //   onPressed: () {
                //     Navigator.of(context).push(
                //         MaterialPageRoute(builder: (context) => SignIn()));
                //   },
                //   child: Text('Already have account'),
                // ),
              ],
            )),
      ),
    );
  }
}
