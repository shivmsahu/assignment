import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'home.dart';
import 'signup.dart';
import 'package:regexed_validator/regexed_validator.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  var _formkey = GlobalKey<FormState>();
  String errorMessage = '';
  bool _obsecureText = true;
  final _email = TextEditingController();
  final _password = TextEditingController();
  bool loading = false;

  Future<FirebaseUser> login(String email, String pass) async {
    FirebaseAuth _auth = FirebaseAuth.instance;

    try {
      AuthResult result =
          await _auth.signInWithEmailAndPassword(email: email, password: pass);
      FirebaseUser user = result.user;
      return user;
    } catch (e) {
      print(e);
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.greenAccent,
      body: SafeArea(
        child: Form(
            key: _formkey,
            child: ListView(
              children: <Widget>[
                Stack(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(25, 100, 0, 0),
                      child: Text(
                        'Auth',
                        style: TextStyle(
                          fontSize: 80,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(25, 170, 0, 0),
                      child: Text(
                        'App',
                        style: TextStyle(
                          fontSize: 80,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.white,
                  ),
                  padding: const EdgeInsets.all(20.0),
                  margin: EdgeInsets.all(20),
                  child: Column(children: [
                    TextFormField(
                      decoration: InputDecoration(labelText: 'Email'),
                      validator: (input) {
                        if (!validator.email(input)) {
                          return 'Please enter correct email address';
                        }
                      },
                      keyboardType: TextInputType.emailAddress,
                      controller: _email,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 85,
                          child: TextFormField(
                            decoration: InputDecoration(labelText: 'Password'),
                            validator: (input) {
                              if (input.isEmpty) {
                                return 'Please enter a password';
                              }
                            },
                            controller: _password,
                            obscureText: _obsecureText,
                          ),
                        ),
                        Expanded(
                          flex: 15,
                          child: IconButton(
                              icon: Icon(
                                  _obsecureText ? Icons.lock : Icons.lock_open),
                              onPressed: () {
                                setState(() {
                                  if (_obsecureText)
                                    _obsecureText = false;
                                  else
                                    _obsecureText = true;
                                });
                              }),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Text(
                        errorMessage,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ]),
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 5,
                    ),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  height: 60,
                  margin: EdgeInsets.fromLTRB(50, 10, 50, 10),
                  child: Material(
                    borderRadius: BorderRadius.circular(30),
                    child: InkWell(
                      child: Center(
                        child: Text(
                          loading ? 'Loading...' : 'Sign in',
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      onTap: () async {
                        if (_formkey.currentState.validate()) {
                          setState(() {
                            loading = true;
                          });
                          final email = _email.text.toString().trim();
                          final pass = _password.text.toString().trim();

                          FirebaseUser user = await login(email, pass);

                          if (user != null) {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    Home(name: user.displayName)));
                          } else {
                            setState(() {
                              errorMessage = 'Email/Password does not match';
                              loading = false;
                            });
                          }
                        }
                      },
                    ),
                  ),
                ),
                Container(
                  height: 60,
                  margin: EdgeInsets.fromLTRB(30, 10, 30, 10),
                  child: Material(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(30),
                    child: InkWell(
                      child: Center(
                        child: Text(
                          'New to this app',
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => SignUp()));
                      },
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
