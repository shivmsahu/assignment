import 'package:flutter/material.dart';
import 'signin.dart';

class Home extends StatelessWidget {
  String name;
  Home({this.name});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.greenAccent,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Welcome',
                style: TextStyle(
                  fontSize: 70,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                name,
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 5,
                  ),
                  borderRadius: BorderRadius.circular(30),
                ),
                height: 60,
                margin: EdgeInsets.fromLTRB(50, 10, 50, 10),
                child: Material(
                  borderRadius: BorderRadius.circular(30),
                  child: InkWell(
                    child: Center(
                      child: Text(
                        'Logout',
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    onTap: () async {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => SignIn()));
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
